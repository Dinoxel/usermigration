<?php
/**
 * User Migration
 * User Migration Hooks
 *
 * @author		Foxlit
 * @copyright	(c) 2010 Foxlit
 * @license		GNU General Public License v2.0 or later
 * @package		User Migration
 *
**/

class UserMigrationHooks {
	/**
	 * User Login Form Hook
	 *
	 * @access	public
	 * @param	object	UserloginTemplate Object
	 * @return	object	Manipulated UserloginTemplate Object
	 */
	static public function onUserLoginForm(&$template) {
		global $wgOut;

		$wgOut->addHTML('<p>Have a legacy Wikia account? <a href="/Special:ClaimExternalAccount" title="Special:ClaimExternalAccount">Reclaim it</a></p>');

		return $template;
	}
}
?>